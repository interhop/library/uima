/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.impl;

/**
 * A Condition has a filter operator and a condition value.
 */
public class Condition {

   private final FilterOp operatorType;

   private final String value;

   /**
    * creates a new condition object with a filter operator and a condition
    * value.
    * 
    * @param operator
    *           filter operator
    * @param value
    *           condition value
    */
   public Condition(FilterOp operator, String value) {
      this.operatorType = operator;
      this.value = value;
   }

   /**
    * Returns the condition operator type.
    * 
    * @return returns the condition operator type.
    */
   public FilterOp getConditionType() {
      return this.operatorType;
   }

   /**
    * Returns the condition value.
    * 
    * @return Returns the condition value
    */
   public String getValue() {
      return this.value;
   }

   /**
    * Returns the FilterOperator for the given String operator. Allowed String
    * operators are: NULL, NOT_NULL, EQUALS, NOT_EQUALS, LESS, LESS_EQ, GREATER,
    * GREATER_EQ
    * 
    * @param operator
    *           operator as String
    * @return FilterOperator for the given String operator
    * 
    */
   public static final FilterOp getOperator(String operator) {
      if (operator.equals("NULL")) {
         return FilterOp.NULL;
      } else if (operator.equals("NOT_NULL")) {
         return FilterOp.NOT_NULL;
      } else if (operator.equals("EQUALS")) {
         return FilterOp.EQUALS;
      } else if (operator.equals("NOT_EQUALS")) {
         return FilterOp.NOT_EQUALS;
      } else if (operator.equals("LESS")) {
         return FilterOp.LESS;
      } else if (operator.equals("LESS_EQ")) {
         return FilterOp.LESS_EQ;
      } else if (operator.equals("GREATER")) {
         return FilterOp.GREATER;
      } else if (operator.equals("GREATER_EQ")) {
         return FilterOp.GREATER_EQ;
      } else {
         return null;
      }
   }
}
