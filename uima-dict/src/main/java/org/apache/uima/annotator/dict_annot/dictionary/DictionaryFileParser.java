/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.dictionary;

import java.io.InputStream;

import org.apache.uima.resource.ResourceInitializationException;

/**
 * The DictionaryFileParser interface defines the method to parse a dictionary
 * file to create a dictionary.
 */
public interface DictionaryFileParser {

   /**
    * parse the given dictionary file and creates a new dictionary using the
    * given dictionary builder.
    * 
    * @param dictionaryFilePath
    *           dictionary XML file path
    * 
    * @param dictionaryFileStream
    *           dictionary XML file stream
    *           
    * @param dictBuilder
    *           dictionary build that should be used to create the dictionary
    * @return returns the created Dictionary
    * @throws ResourceInitializationException
    */
   public Dictionary parseDictionaryFile(String dictionaryFilePath,
         InputStream dictionaryFileStream, DictionaryBuilder dictBuilder)
         throws ResourceInitializationException;
}
