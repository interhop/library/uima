/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.dictionary.impl;

import org.apache.uima.annotator.dict_annot.impl.Condition;
import org.apache.uima.cas.text.AnnotationFS;

/**
 * FeaturePath interface defines the access to the value for a feature path
 * 
 */
public interface FeaturePathInfo {

   /**
    * Returns the value of the given annotation FS for the stored featurePath.
    * 
    * @param annotFs
    *           annotation where the featurePath should be resolved on.
    * 
    * @return featurePath value as String
    */
   public String getValue(AnnotationFS annotFs);

   /**
    * Returns the featurePath as String
    * 
    * @return featurePath string value
    */
   public String getFeaturePath();

   /**
    * Returns true if the featurePath value for the given annotation match the
    * specified condition.
    * 
    * It returns false if no featurePath was specified!
    * 
    * @param annotFS
    *           current annotation to check
    * @param condition
    *           condition for the match
    * 
    * @return returns true if the conditions match the featurePath value
    */
   public boolean match(AnnotationFS annotFS, Condition condition);
}