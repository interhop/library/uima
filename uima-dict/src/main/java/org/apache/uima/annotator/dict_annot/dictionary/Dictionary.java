/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.dictionary;

import java.util.HashSet;
import org.apache.uima.annotator.dict_annot.dictionary.impl.FeaturePathInfo;
import org.apache.uima.cas.text.AnnotationFS;


/**
 * Dictionary interface to work with a dictionary. 
 * The interface defines methods to check if a word or multi-word is available 
 * in the dictionary and to match tokens against the dictionary.
 */
public interface Dictionary {

   /**
    * Checks if the given word is available in the dictionary.
    * 
    * @param word
    *           word to look for
    * 
    * @return returns true if the given word is available in the dictionary,
    *         otherwise false is returned
    */
   public boolean contains(String word);

   /**
    * Checks if the given multi word is available in the dictionary.
    * 
    * @param multiWord
    *           multi word to look for
    * 
    * @return returns true if the given multi word is available in the
    *         dictionary, otherwise false is returned
    */
   public boolean contains(String[] multiWord);

   /**
    * Checks if at the current position in the token array a match in the
    * dictionary is found.
    * 
    * @param pos
    *          current array position 
    * 
    * @param annotFSs 
    *           input annotation FS array
    * 
    * @param featPathInfo 
    *           featurePath information for the matching
    *           
    * @return returns a DictionaryMatch object in case a match was found. If no
    *         match was found, null is returned
    */
   public DictionaryMatch matchEntry(int pos, AnnotationFS[] annotFSs, FeaturePathInfo featPathInfo);
   
   /**
    * Returns the number of entries that are stored in the dictionary.
    * 
    * @return number of entries
    */
   public int getEntryCount();
   
   /**
    * Returns the type name which should use to create annotations
    * 
    * @return type name
    */
   public String getTypeName();
   
   /**
    * Returns the language of this dictionary
    * 
    * @return dictionary language
    */
   public String getLanguage();
   
   /**
    * Returns a set of stop words that will not be considered for matches
    * 
   * @return the the stopwords
   */
  public HashSet<String> getStopWords();
}
