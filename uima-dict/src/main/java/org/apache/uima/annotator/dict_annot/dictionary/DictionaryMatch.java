/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.dictionary;

/**
 * The DictionaryMatch interface defines the access to a dictionary match.
 */
public interface DictionaryMatch {

   /**
    * Returns the dictionary match entry meta data.
    * 
    * @return The dictionary match entry meta data is returned
    */
   public EntryMetaData getMatchMetaData();

   /**
    * Returns the length of the match.
    * 
    * @return returns 1 for a single word match. In case of a multi word match,
    *         the multi word token count is returned.
    */
   public int getMatchLength();
}
