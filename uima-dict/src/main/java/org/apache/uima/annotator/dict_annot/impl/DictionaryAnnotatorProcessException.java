/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.impl;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;

/**
 * DictionaryAnnotatorProcessException is thrown if an annotator processing error
 * occurs.
 */
public class DictionaryAnnotatorProcessException extends
      AnalysisEngineProcessException {

   private static final long serialVersionUID = 8446506776134629247L;

   /**
    * Creates a new exception with a the specified message from the
    * DictionaryAnnotator message catalog.
    * 
    * @param aMessageKey
    *           an identifier that maps to the message for this exception. The
    *           message may contain place holders for arguments as defined by
    *           the {@link java.text.MessageFormat MessageFormat} class.
    * @param aArguments
    *           The arguments to the message. <code>null</code> may be used if
    *           the message has no arguments.
    */
   public DictionaryAnnotatorProcessException(String aMessageKey,
         Object[] aArguments) {
      super(DictionaryAnnotator.MESSAGE_DIGEST, aMessageKey, aArguments);
   }
}
