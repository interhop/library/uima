/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.dictionary;

import java.util.HashSet;

/**
 * The Dictionary builder interface define the methods to create a new dictionary.
 */
public interface DictionaryBuilder {

  /**
   * @return returns the Dictionary object if the dictionary could be created.
   */
  public Dictionary getDictionary();

  /**
   * Adds a new word to the dictionary.
   * 
   * @param word word that should be added.
   */
  public void addWord(String word);

  /**
   * Set the dictionary properties, this method have to be called before words can be added to the
   * dictionary.
   * 
   * @param language dictionary language
   * 
   * @param typeName type name for the dictionary content
   * @param caseNormalization case normalization settings
   * @param multiWordEntries multi-word entries setting
   * @param multiWordSeparator multi-word entry separator
   */
  public void setDictionaryProperties(String language, String typeName, boolean caseNormalization,
      boolean multiWordEntries, String multiWordSeparator, boolean accentNormalization,
      HashSet<String> stopWords, String snowballStemmer);

}
