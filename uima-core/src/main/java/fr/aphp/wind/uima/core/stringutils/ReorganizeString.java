/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.core.stringutils;

public class ReorganizeString {
	private String goal;
	private String original;
	private int substitutionNumber = 0;

	public ReorganizeString(String original, String goal) {
		this.original = original;
		this.goal = goal;
	}
	
	public void process(){
		Integer step = 0;
		while(true){
			if(getWindOriginal(step).equals(getWindGoal(step))){
				//les morceaux sont égaux
				}else if(substitute(getWindGoal(step)).equals(getWindOriginal(step))){//une substitution rétablit...
				original =  transform(step, original, getWindGoal(step));
				substitutionNumber++;
			}
			step++;//on avance dans le cadre
			if(step + 1 >= original.length() ){break;}//la fin approche



		}

		
	}
	
	private String getWindOriginal(final Integer i){
		return this.original.substring(i, i+2);
	}
	
	private String getWindGoal(final Integer i){
		return this.goal.substring(i, i+2);
	}
	
	private String substitute(String db){
		return String.format("%s%s", db.charAt(1), db.charAt(0));
	}
	
	private String transform(Integer step, String origin, String replacement){
		return origin.substring(0,step) + replacement + origin.substring(step+2, origin.length());
	}
	
	public Integer getSubstitutionNumber(){
		return this.substitutionNumber;
	}
	
	public String getReorganizedString(){
		return this.original;
	}
		
	

}
