/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.core.stringutils;

public  class  ListDeid {
	public static String[] PREFIX_WORDS = { "interne","cs", "uhcd", "chu", "externe", "pu-ph", "puph", "cca","ph", "iao"
			,  "pu", "phu","praticien", "hospitalier", "mt", "hu","sexe"
			, "prenom", "prénom", "nom", "confrère", "diététicien","ide","dr", "opérateurs", "opérateur","assistant", "pr", "dr.", "pr.", "m", "d", "mr"
			, "mr.", "mme", "professeur", "prof", "prof.", "madame", "mademoiselle", "docteur"
			, "docteurs", "monsieur", "melle", "mlle", "mrs", "mr", "ms", "né", "date" , "age","me" };
	
	public static String[] STOP_WORDS = { "de", "le", "la", "des", "du", "les", "l", "d", "et", "ou", "avec", "dans", "du",
	"pour" };
	public static String[] PREFIX_CITY_REGEX = { "(?i)(rue|boulevard|bvd|avenue|passage|pass\\.)(?-i)", "loge", "déménag",
			"habit", "r[eé]sid", "au", "vers", "à", "pour", "sur", "près de", "du c[oô]t[eé] de", "origin", "lieux", "(?i)cedex", "BP" };

	public static String[] PREFIX_COUNTRY_REGEX = { "a fuit le", "déménag", "la", "transit", "habit", "r[eé]sid", "près de",
			"du c[oô]t[eé] de", "du", "de", "au", "en", "le", "la" };


	public static String[] AROUND_UCUM_REGEX = { "\\d+", "\\p{Punct}" };

	public static String[] PREFIX_STREET_REGEX = { "\\badresse", "\\bloge", "\\bdéménag", "\\bhabit", "\\br[eé]sid",
			"\\bretourne", "\\bvit\\b", "\\bprès de", "du c[oô]t[eé] de" };
	
	public static String[] AROUND_TABLE_REGEX = { "[\\|]", "[¦]" };
	
	public static String[] SPACIAL_WORDS = {"rue", "résidence", "ambassade","boulevard"
			, "boulevar", "etage", "cite", "allee"
			, "impasse", "passage", "avenue", "batiment",  "blvd",  "chemin"
			,  "hameau", "square", "cours", "place", "porte", "route", "villa"
			, "appt", "chez",  "place","maison", "medical", "quai", "zone", "app", "apt", "bis"
			, "etg", "imm", "imp", "pte",  "rue", "via", "av", "bd", "bv", "service", "sce" 
			, "hopital", "clinique", "centre", "cabinet"};
	
	public static String RECUP_FORBIDEN_REGEX = "(?i)(hop|cab|hospi|départ)";
	
	public static String DATE_DECRET_REGEX = "(?i)(la loi|d[ée]cret|DHOS|DGS|CTIN|circulaires|OMS)";
	
	public static String[] RECUP_BREAK_WORD = {"à", "de", "pour", "du", ":","en"};
	
	public static String[] RECUP_MALADIE = {"echelle de", "maladie de", "syndrome de", "score", "test de", "signe de", "sonde de"
			, "mutation de", "pose de","phénomène de"};

	public static String[] DECLENCHEUR_NAME_REGEX = {"(?i)(?u)(operateur ?: ?|anesthesiste ?: ?|dieteticien ?: ?|dieteticienne ?: ?|)" };
	
	public static String RECUP_LAST_NAME_REGEX = "([\\p{Lu}]+[ |-]*[\\p{Lu}]){1,4}";
	
	public static String RECUP_FIRST_NAME_REGEX = "((?:[\\p{Lu}]\\.|[\\p{Lu}][\\p{Ll}]+)(?:(?:[-| ][\\p{Lu}][\\.])|[-| ][\\p{Lu}][\\p{Ll}]+)*){1,4}";
	//en, au, date, du, à, sur, le, la, les, d', de, par, un, une, vos, votre
	public static String[] NAME_BAD_SHORT_REGEX = {	
			"(?i)[\\s\\S]*\\bau\\s*",
			"[\\s\\S]*\\bdu\\s*",
			"(?i)[\\s\\S]*\\ben\\s*",
			"(?i)[\\s\\S]*\\bà\\s*",
			"(?i)[\\s\\S]*\\bsur\\s*",
			"(?i)[\\s\\S]*\\ble\\s*",
			"(?i)[\\s\\S]*\\bl'\\s*",
			"(?i)[\\s\\S]*\\bla\\s*",
			"(?i)[\\s\\S]*\\bles\\s*",
			"[\\s\\S]*\\bd'\\s*",
			"[\\s\\S]*\\bde\\s*",
			"[\\s\\S]*\\bpar\\s*",
			"(?i)[\\s\\S]*\\bun\\s*",
			"(?i)[\\s\\S]*\\bune\\s*",
			"(?i)[\\s\\S]*\\bvos\\s*",
			"(?i)[\\s\\S]*\\bvotre\\s*"};
	public static String[] NAME_BAD_SHORT_FOL_REGEX = {	
			"\\s*\\bet\\b[\\s\\S]*",
			"\\s*\\ble\\b[\\s\\S]*",
			"\\s*\\bles\\b[\\s\\S]*",
			"\\s*\\bla\\b[\\s\\S]*",
			"\\s*\\bl'\\b[\\s\\S]*",
			"\\s*\\b\\d\\d?\\b[\\s\\S]*"
			};
	//clinique, unité, hopital, hopitaux de, hopital du, institut, résidence, droit, gauche,, batiment
	public static String[] NAME_BAD_SMEDIUM_REGEX = {	
			"(?i)(?u)[\\s\\S]*\\bclinique[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bunité[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bhopital[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bhopitaux[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bhopital[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\binstitut[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\brésidence[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bdroit[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bdroite[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bgauche[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bbatiment[\\s\\S]*"};
	
	// droit, gauche, négatif, positif
	public static String[] NAME_BAD_SMEDIUM_FOL_REGEX = {	
			"(?i)(?u)[\\s\\S]*\\bdroit[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bgauche[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bnégat[\\s\\S]*",
			"(?i)(?u)[\\s\\S]*\\bposit[\\s\\S]*"};
	// droit, gauche, négatif, positif
	
		public static String[] CITY_GOOD_SMEDIUM_FOL_REGEX = {	
				"(?i)(?u)[\\s]*,[\\s]*le[\\s\\S]*"};
}
