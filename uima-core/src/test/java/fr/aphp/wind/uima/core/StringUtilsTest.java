/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.core;

import fr.aphp.wind.uima.core.stringutils.StringUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;




/**
 * Unit test for simple App.
 */
public class StringUtilsTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public StringUtilsTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( StringUtilsTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testFirstLowerCase()
    {
        assertTrue( StringUtils.isFirstLowerCase("maman") );
        assertFalse( StringUtils.isFirstLowerCase("1maman") );

    }
    
    /**
     * Rigourous Test :-)
     */
    public void testFirstUpperCase()
    {
        assertTrue( StringUtils.isFirstUpperCase("Maman") );
    }
    
    public void testisDigitPunct()
    {
        assertTrue( StringUtils.isDigitPunct("1".substring(0)) );
        assertFalse( StringUtils.isDigitPunct("a") );
    }
}
