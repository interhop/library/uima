/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.mapper;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

public class ChunckAnnotator extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

		for (fr.aphp.wind.uima.type.GeneralFirstNameChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralFirstNameChunk.class)) {
			chunck.setChunkValue("FNAME");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralLastNameChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralLastNameChunk.class)) {
			chunck.setChunkValue("LNAME");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralDateChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralDateChunk.class)) {
			chunck.setChunkValue("DATE");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralEMailChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralEMailChunk.class)) {
			chunck.setChunkValue("EMAIL");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralNbIppChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralNbIppChunk.class)) {
			chunck.setChunkValue("IPP");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralNbNdaChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralNbNdaChunk.class)) {
			chunck.setChunkValue("NDA");
			chunck.addToIndexes();
		}

		for (fr.aphp.wind.uima.type.GeneralPhoneNumberChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralPhoneNumberChunk.class)) {
			chunck.setChunkValue("PHONE");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralNbSecuChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralNbSecuChunk.class)) {
			chunck.setChunkValue("NIR");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralZipCodeChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralZipCodeChunk.class)) {
			chunck.setChunkValue("ZIP");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralCityChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralCityChunk.class)) {
			chunck.setChunkValue("CITY");
			chunck.addToIndexes();
		}
		for (fr.aphp.wind.uima.type.GeneralStreetChunk chunck : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralStreetChunk.class)) {
			chunck.setChunkValue("STREET");
			chunck.addToIndexes();
		}

	}

}
