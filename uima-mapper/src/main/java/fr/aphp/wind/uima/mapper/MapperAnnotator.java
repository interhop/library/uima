/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.mapper;

import java.util.regex.Pattern;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import fr.aphp.wind.uima.type.SourceDocumentInformation;

public class MapperAnnotator extends JCasAnnotator_ImplBase {
    private Pattern pat;
    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
	// TODO Auto-generated method stub
	super.initialize(context);
	String componndWordPattern = "[\\p{L}]+(-[\\p{L}]+)+";
	pat = Pattern.compile(componndWordPattern);
    }
    

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {

	/* all sentence s */

	for (de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence sentence : JCasUtil
		.select(aJCas,
			de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence.class)) {
	    de.unihd.dbs.uima.types.heideltime.Sentence sd = new de.unihd.dbs.uima.types.heideltime.Sentence(
		    aJCas, sentence.getBegin(), sentence.getEnd());
	    sd.addToIndexes();
	    /* all Noun Phrases within that sentence */
	    for (Token dkproToken : JCasUtil.selectCovered(aJCas, Token.class, sentence)) {

		de.unihd.dbs.uima.types.heideltime.Token tok = new de.unihd.dbs.uima.types.heideltime.Token(
			aJCas, dkproToken.getBegin(), dkproToken.getEnd());
		POS pos = dkproToken.getPos();
		String posValue;
		if (pos == null||pos.getPosValue() == null) {
		    posValue = "UNKNOWN";
		}else {
		    posValue = pos.getPosValue();
		}
		tok.setPos(posValue);
		tok.addToIndexes();

		// Trick: for dictionnary annotator, in order it breaks
		// coumpound words, add dedicated tokens

		if (pat.matcher(dkproToken.getCoveredText()).matches()) {
//		     System.out.println(String.format("Mot composé: %s",
//		     dkproToken.getCoveredText()));
		    int begin = 0;
		    int end = 0;
		    for (   int index = dkproToken.getCoveredText().indexOf("-");
			    index >= 0; 
			    index = dkproToken.getCoveredText().indexOf("-", index + 1)) {

			Token t = new Token(aJCas, dkproToken.getBegin() + begin,
				dkproToken.getBegin() + index);
			t.addToIndexes();
//			System.out.println(String.format("Mot décomposé: %s",
//			 t.getCoveredText()));
			begin = index + 1;
			end = index;

		    }
		    Token t = new Token(aJCas, dkproToken.getBegin() + begin, dkproToken.getEnd());
		    t.addToIndexes();
//		     System.out.println(String.format("Mot décomposé: %s",
//		     t.getCoveredText()));

		}

	    } /* for each noun phrase within the sentence */
	} /* for each sentence */
	//System.out.println(getUri(aJCas));
    }

    private String getUri(JCas aJCas) {
      FSIterator<Annotation> a = aJCas.getAnnotationIndex(SourceDocumentInformation.type).iterator();
      SourceDocumentInformation b = null;
      if (a.hasNext()) {
        b = (SourceDocumentInformation) a.next();
        return b.getUri();
      }
      // file:/tmp/input/CR-CONS_24081078.txt
      return DocumentMetaData.get(aJCas).getDocumentUri().replaceAll(".*?([^/]+).txt$", "$1");
    }
}
