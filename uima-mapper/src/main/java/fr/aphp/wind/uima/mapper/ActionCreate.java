/**
 * This file is part of UIMA-APHP.
 * UIMA-APHP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * UIMA-APHP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.mapper;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.cas.TOP;

public class ActionCreate implements Action {
	private String oldType;
	private String[] newTypes;

	public String getOldType() {
		return oldType;
	}

	public void setOldType(String oldType) {
		this.oldType = oldType;
	}

	public String[] getNewTypes() {
		return newTypes;
	}

	public void setNewTypes(String[] newTypes) {
		this.newTypes = newTypes;
	}

	public ActionCreate(String oldType, String[] newTypes) {
		this.oldType = oldType;
		this.newTypes = newTypes;
	}

	public ActionCreate() {

	}

	@Override public void process(CAS aCas) {
		Type annotationType = CasUtil.getType(aCas, oldType);
		Type newTypeAnn;
		for (AnnotationFS sp : CasUtil.select(aCas, annotationType)) {
			for (String newType : newTypes) {
				if (!newType.equals(oldType)) {
					newTypeAnn = CasUtil.getType(aCas, newType);
					AnnotationFS b = aCas.createAnnotation(newTypeAnn, sp.getBegin(), sp.getEnd());
					((TOP) b).addToIndexes();
				}
			}
		}
	}

}
