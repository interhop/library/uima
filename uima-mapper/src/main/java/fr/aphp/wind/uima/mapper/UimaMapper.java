/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.mapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UimaMapper extends JCasAnnotator_ImplBase {

	public static final String PARAM_CONFIG_FILE_PATH = "configfFilePath";
	@ConfigurationParameter(name = PARAM_CONFIG_FILE_PATH, mandatory = false)
	private String configfFilePath;
	
	private List<Action> actionList ;

	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		super.initialize(aContext);
		actionList = new ArrayList<Action>();
		ObjectMapper mapper = new ObjectMapper();

		try {

			JsonNode json = mapper.readTree(new File(configfFilePath));
			Iterator<JsonNode> tmp;
			//WARNING: does not copy the features ! To be done !

			if (json.has("OVERWRITE")) {

				 tmp = json.get("OVERWRITE").elements();
				while (tmp.hasNext()) {
					JsonNode el = tmp.next();
					actionList.add(mapper.treeToValue(el, ActionOverwrite.class));
				}
			}
			
			if (json.has("CREATE")) {
				 tmp = json.get("CREATE").elements();
				while (tmp.hasNext()) {
					JsonNode el = tmp.next();
					actionList.add(mapper.treeToValue(el, ActionCreate.class));
				}
			}
			
			if (json.has("CESURE")) {
				actionList.add(mapper.treeToValue(json.get("CESURE").elements().next(), ActionDeduplicate.class));
		
			}
			
			if (json.has("DEDUPLICATE")) {

					actionList.add(mapper.treeToValue(json.get("DEDUPLICATE").elements().next(), ActionDeduplicate.class));
			
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

		for (Action action : actionList) {
			action.process(aJCas.getCas());
		}
	}

}
