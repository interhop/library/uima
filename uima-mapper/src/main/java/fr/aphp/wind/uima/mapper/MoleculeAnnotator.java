/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.mapper;

import java.util.ArrayList;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

import fr.aphp.wind.uima.core.casutils.CasTools;

public class MoleculeAnnotator extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

		ArrayList<String> annos = new ArrayList<String>();
		annos.add("fr.aphp.wind.uima.type.GeneralStreetChunk");
		annos.add("fr.aphp.wind.uima.type.GeneralFirstNameChunk");
		annos.add("fr.aphp.wind.uima.type.GeneralLastNameChunk");
		annos.add("fr.aphp.wind.uima.type.GeneralCityChunk");
		
		CasTools.explodeMolecules(aJCas, annos);
		
	}

}
