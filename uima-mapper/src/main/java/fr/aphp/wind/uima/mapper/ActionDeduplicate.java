/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.mapper;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.cas.TOP;

public class ActionDeduplicate implements Action {

	private String[] deduplicate;

	public ActionDeduplicate(String[] supraTypes) {

		this.deduplicate = supraTypes;
	}

	public ActionDeduplicate() {
	}

	public String[] getDeduplicate() {
		return deduplicate;
	}

	public void setDeduplicate(String[] supraTypes) {
		this.deduplicate = supraTypes;
	}

	@Override
	public void process(CAS aCas) {
//will keep only the largest anntation

		for (String annoToDeduplicate : deduplicate) {
			Type annotationType = CasUtil.getType(aCas, annoToDeduplicate);
			for (AnnotationFS sp : CasUtil.select(aCas, annotationType)) {
				AnnotationFS biggest = null;
					for (AnnotationFS sc : CasUtil.selectCovering(aCas, annotationType, sp)) {
						if(biggest==null){
							biggest = sc;
							continue;
						}
						if(sc.getBegin() < biggest.getBegin() || sc.getEnd() > biggest.getEnd()){
							biggest = sc;
						}
					}
					for (AnnotationFS sc : CasUtil.selectCovering(aCas, annotationType, sp)) {
						if(!sc.equals(biggest)){
						TOP test = (TOP) sc;
						test.removeFromIndexes();
						}
					}
			}
		}
	}
}
