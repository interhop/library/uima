/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.eds.DeidTools;

public class DeidDateExtractor {

  public static String extract(String entry) {
    String format =  "dd/MM/yyyy";
    if(entry == null) {
	return format;
    }
    if (entry.matches("^\\p{L}+$")) {// mois (octobre)
      format = "MMMM";
    } else if (entry.matches("^\\d{1,2}$")) {// jour (01)
      format = "dd";
    } else if (entry.matches("^\\d{4}$")) {// Annee (2015)
      format = "yyyy";
      } else if (entry.matches("^\\d{4}.\\d{2}$|^\\d{2}.\\d{4}$")) {// Annee (2015/02)
        format = "MM/yyyy";
    }
    return format;
  }

}
