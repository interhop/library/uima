/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.eds.DeidTools;

import java.util.LinkedList;

public class DeidTools {

    public static String replaceOffsets(String monCR, Integer[] begins, Integer[] ends,
	    String[] replacements) {
	if (monCR ==null || begins == null || ends == null || replacements == null)
	    return monCR;
	// checks
	if (begins.length != ends.length) {
	    throw new RuntimeException("offsets should be the same legnth");
	}
	if (begins.length != replacements.length) {
	    throw new RuntimeException("replacements offsets should be the same");
	}

	LinkedList<String> pieces = new LinkedList();
	Integer bIdx = 0;
	String piece = null;
	try {
	for (int i = 0; i < replacements.length; i++) {
	    if(bIdx > begins[i]) {
		bIdx = Math.max(ends[i], bIdx);
		continue;
	    }
	    piece = monCR.substring(bIdx, begins[i]);
	    pieces.add(piece);
	    pieces.add(replacements[i]);
	    bIdx = ends[i];
	}

	pieces.add(monCR.substring(bIdx, monCR.length()));
	}catch(Exception e) {
	    return "ERROR - "+ e.getMessage();
	}
	return String.join("", pieces);
    }

}
