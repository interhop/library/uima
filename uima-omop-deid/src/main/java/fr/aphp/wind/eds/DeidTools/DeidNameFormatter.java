/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.eds.DeidTools;

import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DeidNameFormatter {

    public static String format(String name, String originalName) {
	if(originalName == null || name == null) {
	    return null;
	}
	if (isStringUpperCase(originalName)) {
	    return name.toUpperCase();
	}
	name = name.toLowerCase();
	Matcher m = Pattern.compile("^\\p{L}| \\p{L}|-\\p{L}").matcher(name);

	StringBuilder sb = new StringBuilder();
	int last = 0;
	while (m.find()) {
	    sb.append(name.substring(last, m.start()));
	    sb.append(m.group(0).toUpperCase());
	    last = m.end();
	}
	sb.append(name.substring(last));
	return sb.toString().trim();
    }

 private static boolean isStringUpperCase(String str){
        
        //convert String to char array
        char[] charArray = str.toCharArray();
        
        for(int i=0; i < charArray.length; i++){
            
            //if any character is not in upper case, return false
            if( Character.isAlphabetic(charArray[i]) && !Character.isUpperCase( charArray[i] ))
                return false;
        }
        
        return true;
    }

}
