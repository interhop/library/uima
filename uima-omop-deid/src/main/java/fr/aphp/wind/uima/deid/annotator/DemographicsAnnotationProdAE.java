/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.annotator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.aphp.wind.uima.core.casutils.CasTools;
import fr.aphp.wind.uima.core.stringutils.StringDistance;
import fr.aphp.wind.uima.type.NoteDeidDemographics;

public class DemographicsAnnotationProdAE extends JCasAnnotator_ImplBase {
	private Logger logger = Logger.getLogger(getClass().getName());
	private StringDistance distKernel ;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		distKernel = new StringDistance();
	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		// ALGO
		// 1. FOREACH ANNOT_NAME
		// 2. CREATE TYPE
		// 3. CREATE ANNOTATION
		ObjectMapper mapper = new ObjectMapper();
		NoteDeidDemographics demographicsDocumentAnnotation = JCasUtil.selectSingle(aJCas,
				fr.aphp.wind.uima.type.NoteDeidDemographics.class);

		String docText = aJCas.getDocumentText();

		// Name: matchName()
		// String: exactMatch
		// Phone: matchNumber()
		// Number: matchNumber()

		try {
			JsonNode json = mapper.readTree(demographicsDocumentAnnotation.getJson());
			Iterator<JsonNode> demographics = json.elements();

			while (demographics.hasNext()) {
				JsonNode tmp2 = demographics.next();
				if(tmp2.get("values")==null) {
					return;
				}
				Iterator<JsonNode> values = tmp2.get("values").elements();
				String annot = tmp2.get("type").textValue();

				while (values.hasNext()) {
					JsonNode tmp = values.next();

					String value = tmp.get("value").textValue();
					value = prepareValue(value);
					String type = tmp.get("dataType").textValue();

					// very short things are not taken in consideration
					if (value.length() <= 2)
						continue;

					Pattern pPattern = Pattern.compile("(?i)" + Pattern.quote(value));
					Type tokenType;
					Matcher matcher = pPattern.matcher(docText);
					int pos = 0;
					tokenType = CasUtil.getAnnotationType(aJCas.getCas(), annot);

					while (pos < docText.length() && matcher.find(pos)) {
						int annotStart, annotEnd;
						annotStart = matcher.start();
						annotEnd = matcher.end();

						// create Annotation in CAS
						FeatureStructure fs = aJCas.getCas().createAnnotation(tokenType, annotStart,
								annotEnd);

						aJCas.getIndexRepository().addFS(fs);
						pos = annotEnd;
						logger.debug(type + " found: >" + fs + "<");
					}

					if (type.equals("Name")) {// levenstein pondéré, gesion tiret dans le mot composé

						if (value.trim().length() <= 1 || !value.matches("[\\s\\S]*\\p{L}[\\s\\S]*")) {// sometime only one digit
							continue;
						}
						ArrayList<String> bagSearch = splitWordsToFind(value);
						Collection<Token> tokensToMine = JCasUtil.select(aJCas,
								Token.class);
						int distanceInterWords = 2;
						int sizeSmallWord = 3;
						int bagSizeMinimal = 1;
						ArrayList<ArrayList<Token>> bagResults = getListMatchingBags(
								bagSearch, tokensToMine, distanceInterWords, sizeSmallWord,
								bagSizeMinimal);
						for (ArrayList<Token> bagResult : bagResults) {
							FeatureStructure fs = aJCas.getCas().createAnnotation(tokenType,
									bagResult.get(0).getBegin(),
									bagResult.get(bagResult.size() - 1).getEnd());
							aJCas.getIndexRepository().addFS(fs);
							logger.debug(type + " found: >" + CasTools.getTextContent(fs) + "<");

						}
					}
				}

			}
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage() + aJCas.getDocumentText() +  JCasUtil.selectSingle(aJCas,
					fr.aphp.wind.uima.type.NoteDeidDemographics.class).getJson() );
		}

	}

	private String prepareValue(String value) {
		return value.replaceAll("^(\\s+)|(\\s+)$", "");
	}

	private ArrayList<String> splitWordsToFind(String text) {
	// from https://stanfordnlp.github.io/CoreNLP/api.html
		String[] tmp = text.split("[^\\p{L}]+");
		ArrayList<String> tmp2 = new ArrayList<String>();
		for(String a : tmp) {
			tmp2.add(a);
		}
		return tmp2;
	}

	@SuppressWarnings({  "deprecation" })
	private ArrayList<ArrayList< Token>> getListMatchingBags(
			final ArrayList<String> wordsToFind,
			final Collection< Token> tokensToMine,
			final int distanceInterWords, final int sizeSmallWord, final int bagSizeMinimal) {
		ArrayList< Token> subResult = new ArrayList<Token>();
		for (String word : wordsToFind) {// for1
			for ( Token token : tokensToMine) {// for2
				if (distKernel.isSimilar(word, token.getCoveredText(), true)) {
					subResult.add(token);
				}
			} // end for2
		} // end for1
		// group by step
		ArrayList<ArrayList< Token>> result = new ArrayList<ArrayList<Token>>();
		if (subResult.size() == 0) {
			return result;
		}
		int rankActual = 0;
		int rankPreceding = 0;
		boolean flagFirstTime = true;
		ArrayList<Token> bagTmp = new ArrayList<Token>();
		for ( Token tokenToMine : tokensToMine) {// foreach token of text
			rankActual++;
			for ( Token tokenMatched : subResult) {
				if (tokenMatched.getBegin() == tokenToMine.getBegin()) {// they are same word
					if (flagFirstTime || (rankActual - rankPreceding <= distanceInterWords)) {
						flagFirstTime = false;
						rankPreceding = rankActual;
						bagTmp.add(tokenMatched);
					} else {
						result.add(bagTmp);
						bagTmp = new ArrayList<Token>();
						bagTmp.add(tokenMatched);
						rankPreceding = rankActual;
					}
				}
			}
		}
		if (bagTmp.size() > 0) {
			result.add(bagTmp);
		}

		// reduce step: remove bags that are not relevant
		ArrayList<ArrayList<Token>> result2 = new ArrayList<ArrayList<Token>>();

		for (ArrayList<Token> bag : result) {
			if (isBagAcceptable(bag, wordsToFind)) {// bag of words is acceptable
				result2.add(bag);
			}
		}

		return result2;
	}

	private boolean isBagAcceptable(ArrayList<Token> bagProposed,
									ArrayList<String> wordsToFind) {
		// Case only one word to find
		if (wordsToFind.size() == 1) {
			return true;
		}
		// number letters bag proposed is 60% of words to find
		int proposedSize = 0;
		for ( Token word : bagProposed) {
			proposedSize += word.getEnd() - word.getBegin();
		}
		int toFindSize = 0;
		for (String word : wordsToFind) {
			toFindSize += word.length();
		}
		double value = (double) proposedSize / (double) toFindSize * (double) 100;
		boolean isOk = value >= 70;

		return isOk;
	}
}
