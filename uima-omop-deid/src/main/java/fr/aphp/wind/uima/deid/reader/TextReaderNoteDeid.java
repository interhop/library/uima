/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.reader;


/*
 * Copyright 2010
 * Ubiquitous Knowledge Processing (UKP) Lab
 * Technische Universität Darmstadt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.fit.descriptor.ConfigurationParameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.icu.text.CharsetDetector;

import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.api.resources.CompressionUtils;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import fr.aphp.wind.uima.type.NoteDeidDemographics;
import fr.aphp.wind.uima.type.SourceDocumentInformation;
/**
 * UIMA collection reader for plain text files.
 */

public class TextReaderNoteDeid
	extends TextReader
{
	/**
	 * Automatically detect encoding.
	 *
	 * @see CharsetDetector
	 */
	public static final String ENCODING_AUTO = "auto";

	/**
	 * Name of configuration parameter that contains the character encoding used by the input files.
	 */
	public static final String PARAM_ENCODING = ComponentParameters.PARAM_SOURCE_ENCODING;
	@ConfigurationParameter(name = PARAM_ENCODING, mandatory = true, defaultValue = "UTF-8")
	private String encoding;

	@Override
	public void getNext(CAS aJCas)
		throws IOException, CollectionException
	{
		Resource res = nextFile();
		initCas(aJCas, res);

        try (InputStream is = new BufferedInputStream(
                CompressionUtils.getInputStream(res.getLocation(), res.getInputStream()))) {
            String text;

            if (ENCODING_AUTO.equals(encoding)) {
                CharsetDetector detector = new CharsetDetector();
                text = IOUtils.toString(detector.getReader(is, null));
            }
            else {
                text = IOUtils.toString(is, encoding);
            }
            
            SourceDocumentInformation srcDocInfo = new SourceDocumentInformation(aJCas.getJCas());
            srcDocInfo.setUri(res.getLocation().toString());
           
            ObjectMapper mapper = new ObjectMapper();
            mapper.readerForUpdating(srcDocInfo).readValue(
            		new File(res.getLocation()
            		.replaceAll(".txt$", ".json").replaceAll("^file:","")));
            srcDocInfo.addToIndexes();
            
      		ObjectMapper mapper2 = new ObjectMapper();
            JsonNode json = mapper2.readTree(text);
        	NoteDeidDemographics patFtsName = new NoteDeidDemographics(aJCas.getJCas());
    		if(json.has("Demographics")){
    		mapper2.readerForUpdating(patFtsName).readValue(json.path("Demographics"));
    		patFtsName.addToIndexes();
    		}



            aJCas.setDocumentText(json.path("text").textValue());
            
      

        } catch (CASException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	

}
