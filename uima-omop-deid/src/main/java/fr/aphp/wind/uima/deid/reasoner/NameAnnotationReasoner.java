/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.reasoner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;

import com.google.common.collect.Lists;

import fr.aphp.wind.uima.core.casutils.CasTools;
import fr.aphp.wind.uima.core.stringutils.ListDeid;
import fr.aphp.wind.uima.core.stringutils.StringUtils;

import org.apache.log4j.Logger;

public class NameAnnotationReasoner extends JCasAnnotator_ImplBase {
	
	private Logger logger = Logger.getLogger(getClass().getName());	

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

		// GENERAL_DATE_regex
		// unité de mesure apres
		Collection<AnnotationFS> total = CasUtil.select(aJCas.getCas(),
				CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralName"));
		
		
		int i = 0;
		int last = 0;
		for (AnnotationFS generalName : total) {
			// look before
			processBefore(aJCas, generalName.getBegin());
			if (i == 0) {
				last = generalName.getEnd();
				i++;
			} else {
				//look between
				processBetween(aJCas, last, generalName.getBegin());
				last = generalName.getEnd();
				i++;
			}
		}
		if(total.size()>0){//case when nothing inside the document
		processBetween(aJCas, last, aJCas.getDocumentText().length());
		}

		//STEP: LOOK behind prenom / look behnd nom. Si XXXX -> take it
		
		//STEP propagation general noms
		//TODO remettre
//			NoteDeidCU.propagate(aJCas, "fr.aphp.wind.uima.type.GeneralLastNameExtend");
//
//			NoteDeidCU.propagate(aJCas, "fr.aphp.wind.uima.type.GeneralFirstNameExtend");

		//STEP: REVERSE prenom/NOM ...
			
		//STEP: REmove names containing numbers
		
	}
	public void processBefore(JCas aJCas,  int right) {

		String partOfText = aJCas.getDocumentText().substring(0, right);
		int left = 0;
		if (partOfText.lastIndexOf("\n") >= 0) {
			partOfText = partOfText.substring(0, partOfText.lastIndexOf("\n") );
			left =  partOfText.lastIndexOf("\n")  ;
		}
		// left <----\n--> right
		//      ^^^^^
		//         |
		//     all tokens

		List<AnnotationFS> toksBetween = CasUtil.selectCovered(aJCas.getCas(),
				CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token"), left, right);
			int atempt = 0;
			int found = 0;
			boolean foundLn = false;
			boolean foundFn = false;
			for (AnnotationFS tok : Lists.reverse(toksBetween)) {
				atempt++;
				ArrayList<Type> typesToFindAfter = new ArrayList<Type>();
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralFirstName"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralLastName"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralDate"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralDateRegex"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.PatientNbIpp"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralNbNda"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralTitle"));
				ArrayList<String> tokenToExclude = new ArrayList<String>();
				Type tokenANnot = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");
				boolean foundItemResultAfter = CasTools.isNumberWordBetween(aJCas, tok, typesToFindAfter, tokenANnot, 1, 1, tokenToExclude, false);
				boolean isItem = CasTools.hasType(aJCas, "fr.aphp.wind.uima.type.GeneralItemRegex", tok);
				boolean isCommon = CasTools.hasType(aJCas, "fr.aphp.wind.uima.type.GeneralCommon", tok) ;
				boolean isVerb = CasTools.hasType(aJCas, "fr.aphp.wind.uima.type.GeneralVerb", tok) ;

				boolean isGeneral = hasGeneralType(aJCas, tok.getBegin(), tok.getEnd());
				boolean isPunct = Pattern.compile("\\p{Punct}").matcher(tok.getCoveredText()).matches();
				boolean isNum = Pattern.compile("\\d").matcher(tok.getCoveredText()).find();
				boolean isNameAround = 	Arrays.asList(ListDeid.PREFIX_WORDS).contains(tok.getCoveredText().toLowerCase());
				boolean isBreakWord = 	Arrays.asList(ListDeid.RECUP_BREAK_WORD).contains(tok.getCoveredText().toLowerCase());
				boolean	isSpacialAround = 	Arrays.asList(ListDeid.SPACIAL_WORDS).contains(tok.getCoveredText().toLowerCase());
				boolean	isStopAround = 	Arrays.asList(ListDeid.STOP_WORDS).contains(tok.getCoveredText().toLowerCase());
				//TODO: word fallowed by ":" should be avoided
				boolean isFallowedDeuxPoints = false;//A gérer avec les general word avec h
				boolean hasMaxAtempt = (atempt-found >=2);
				
				logger.info(String.format("Tente recup: %s", tok.getCoveredText()));


				// 1a) matches the regex FirstName -> GeneralFirstName
				// 1b) matches the regex LastName -> GeneralLastName
				// 2) not any type covered
				if (( foundItemResultAfter && isItem)|| (isCommon ) || isVerb ||isNum 
						|| isPunct ||  isSpacialAround || isNameAround  || isBreakWord || hasMaxAtempt) {
					logger.info(String.format("Abandon recup: %s", tok.getCoveredText()));
					break;
				} else if (!isGeneral  && ! isStopAround) {
					if( 
							  ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("NPP")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("ADJ")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("NC")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("P")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("PONCT")){
						if (StringUtils.lookFirstName(tok.getCoveredText()) && !foundLn) {
							AnnotationFS b = aJCas.getCas()
									  .createAnnotation(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralFirstNameExtend")
									, tok.getBegin()
									, tok.getEnd());
							((TOP) b).addToIndexes();
							found++;
							foundFn = true;
							logger.info(String.format("Keep recup first name: %s", tok.getCoveredText()));

						}
						if ((StringUtils.lookLastName((tok.getCoveredText())) && !foundFn) 
								|| (StringUtils.doNotContainMaj(tok.getCoveredText())) && tok.getCoveredText().length() >3
								) {
							AnnotationFS b = aJCas.getCas()
									  .createAnnotation(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralLastNameExtend")
									, tok.getBegin()
									, tok.getEnd());
							((TOP) b).addToIndexes();
							found++;
							foundLn = true;
							logger.info(String.format("Keep recup last name: %s", tok.getCoveredText()));

						}
					}else{
						logger.info(String.format("Abandon recup: >%s< (POS:%s)", tok.getCoveredText()
								,  ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos()));
					}
			

				}else{
					logger.info(String.format("Abandon recup: >%s< (general OR is stop)", tok.getCoveredText()));
				}
			}
	}
	public void processBetween(JCas aJCas, int left, int right) {
		if(left<0 || right <0 || left == right || left > right){//aucun espace entre deux noms
			return;
		}
		String partOfText = aJCas.getDocumentText().substring(left, right);
		if (partOfText.indexOf("\n") >= 0) {
			partOfText = partOfText.substring(0, partOfText.indexOf("\n") + 1);
			right = left + partOfText.indexOf("\n") + 1;
		}
		// left <----\n--> right
		//      ^^^^^
		//         |
		//     all tokens

		List<AnnotationFS> toksBetween = CasUtil.selectCovered(aJCas.getCas(),
				CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token"), left, right);

			int atempt = 0;
			int found = 0;
			boolean foundLn = false;
			boolean foundFn = false;
			for (AnnotationFS tok : toksBetween) {
				atempt++;
		
				ArrayList<Type> typesToFindAfter = new ArrayList<Type>();
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralFirstName"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralLastName"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralDate"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralDateRegex"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.PatientNbIpp"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralNbNda"));
				typesToFindAfter.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralTitle"));
				ArrayList<String> tokenToExclude = new ArrayList<String>();
				Type tokenANnot = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");
				boolean foundItemResultAfter = CasTools.isNumberWordBetween(aJCas, tok, typesToFindAfter, tokenANnot, 1, 1, tokenToExclude, false);
				boolean isItem = CasTools.hasType(aJCas, "fr.aphp.wind.uima.type.GeneralItemRegex", tok);
				boolean isCommon = CasTools.hasType(aJCas, "fr.aphp.wind.uima.type.GeneralCommon", tok) ;
				boolean isVerb = CasTools.hasType(aJCas, "fr.aphp.wind.uima.type.GeneralVerb", tok) ;

				boolean isGeneral = hasGeneralType(aJCas, tok.getBegin(), tok.getEnd());
				boolean isPunct = Pattern.compile("\\p{Punct}").matcher(tok.getCoveredText()).matches();
				boolean isNum = Pattern.compile("\\d").matcher(tok.getCoveredText()).find();
				boolean isNameAround = 	Arrays.asList(ListDeid.PREFIX_WORDS).contains(tok.getCoveredText().toLowerCase());
				boolean isBreakWord = 	Arrays.asList(ListDeid.RECUP_BREAK_WORD).contains(tok.getCoveredText().toLowerCase());
				boolean	isSpacialAround = 	Arrays.asList(ListDeid.SPACIAL_WORDS).contains(tok.getCoveredText().toLowerCase());
				boolean	isStopAround = 	Arrays.asList(ListDeid.STOP_WORDS).contains(tok.getCoveredText().toLowerCase());
				//TODO: word fallowed by ":" should be avoided
				boolean hasMaxAtempt = (atempt-found >2);
			
				logger.info(String.format("Tente recup: %s", tok.getCoveredText()));

				// 1a) matches the regex FirstName -> GeneralFirstName
				// 1b) matches the regex LastName -> GeneralLastName
				// 2) not any type covered
				if (( foundItemResultAfter && isItem)|| isCommon  || isVerb ||isNum 
						|| isPunct ||  isSpacialAround || isNameAround  || isBreakWord || hasMaxAtempt) {
					logger.info(String.format("Abandon recup: %s", tok.getCoveredText()));
					break;
				} else 
					if (!isGeneral  && ! isStopAround) {
					if(  toksBetween.size() < 2
							||      ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("NPP")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("ADJ")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("NC")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("P")
							|| ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos().equals("PONCT")){
						if (StringUtils.lookFirstName(tok.getCoveredText()) 
								//&& !foundLn
								) {
							AnnotationFS b = aJCas.getCas()
									  .createAnnotation(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralFirstNameExtend")
									, tok.getBegin()
									, tok.getEnd());
							((TOP) b).addToIndexes();
							found++;
							foundFn = true;
							logger.info(String.format("Keep recup: >%s< (look first name)", tok.getCoveredText()));
						}else{
							logger.info(String.format("Abandon recup: >%s< (not look first name)", tok.getCoveredText()));
						}
						if (StringUtils.lookLastName((tok.getCoveredText())) 
								|| (StringUtils.doNotContainMaj(tok.getCoveredText()) && tok.getCoveredText().length() >3)
								//&& !foundFn
								) {
							AnnotationFS b = aJCas.getCas()
									  .createAnnotation(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralLastNameExtend")
									, tok.getBegin()
									, tok.getEnd());
							((TOP) b).addToIndexes();
							found++;
							foundLn = true;
							logger.info(String.format("Keep recup: >%s< (look last name)", tok.getCoveredText()));
						}else{
							logger.info(String.format("Abandon recup: >%s< (not look last name)", tok.getCoveredText()));
						}
					}else{
						logger.info(String.format("Abandon recup: >%s< (POS:%s)", tok.getCoveredText()
								,  ((de.unihd.dbs.uima.types.heideltime.Token)tok).getPos()));
					}
				}else{
					logger.info(String.format("Abandon recup: >%s< (general OR is stop)", tok.getCoveredText()));
				}
			}
	}

	private boolean hasGeneralType(JCas aJCas, int beginToken, int endToken) {
	List<Type> total = aJCas.getTypeSystem()
		.getProperlySubsumedTypes(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.NoteDeid"));
		total.addAll( aJCas.getTypeSystem()
				.getProperlySubsumedTypes(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.NoteDeidTemp")));
		for (Type a : total) {
			List<AnnotationFS> b = CasUtil.selectCovered(aJCas.getCas(), a, beginToken, endToken);
			List<AnnotationFS> c = CasUtil.selectCovering(aJCas.getCas(), a, beginToken, endToken);
			if (b.size() > 0 || (c.size()>0)) {
				return true;
			}
		}
		return false;
	}

}
