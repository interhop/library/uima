/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.tools;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.SimpleLayout;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.junit.Test;

import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import fr.aphp.wind.uima.deid.tools.TimexTools;
import fr.aphp.wind.uima.mapper.MapperAnnotator;

public class HeideltimeTest {

	private ResultTest getTimex(String text)
			throws ResourceInitializationException
			, AnalysisEngineProcessException
			, InvalidXMLException, IOException {

		ResultTest result = new ResultTest();

		AnalysisEngineDescription stanfordSegmenter = createEngineDescription(StanfordSegmenter.class,
				StanfordSegmenter.PARAM_LANGUAGE, "fr", StanfordSegmenter.PARAM_NEWLINE_IS_SENTENCE_BREAK,
				"TWO_CONSECUTIVE");

		AnalysisEngine ae = AnalysisEngineFactory.createAnalysisEngine(stanfordSegmenter, "defaultView");
		JCas aJCas = ae.newJCas();
		aJCas.setDocumentText(text);

		AnalysisEngineDescription posTaggerNlp = createEngineDescription(OpenNlpPosTagger.class,
				OpenNlpPosTagger.PARAM_LANGUAGE, "en", OpenNlpPosTagger.PARAM_MODEL_LOCATION,
				"fr-pos.bin");

		AnalysisEngineDescription mapperAnnotator = createEngineDescription(MapperAnnotator.class);
		// PIPELINE

		Set<String> set = new HashSet<String>();
		set.add("");

		AnalysisEngineDescription heidelTime = AnalysisEngineFactory.createEngineDescriptionFromPath("HeidelTime.xml");

		SimplePipeline.runPipeline(aJCas, stanfordSegmenter, posTaggerNlp, mapperAnnotator, heidelTime
				// , bratWriter
		);

		for (de.unihd.dbs.uima.types.heideltime.Timex3 timex : JCasUtil.select(aJCas,
				de.unihd.dbs.uima.types.heideltime.Timex3.class)) {
			if (TimexTools.isDeidTimex(timex)) {
				result.addAnnot(timex.getCoveredText());
			}
		} /* for each sentence */
		System.out.println(result);
		return result;
	}

	@Test
	public void testDate()
			throws AnalysisEngineProcessException, ResourceInitializationException, InvalidXMLException, IOException {
		String text = "Je suis né le 13/07/1994 et ma soeur le 03/06/2012";
		ResultTest result = new ResultTest();
		result.addAnnot("13/07/1994");
		result.addAnnot("03/06/2012");
		assertTrue(result.equals(getTimex(text)));

	}

	@Test
	public void testYear()
			throws AnalysisEngineProcessException, ResourceInitializationException, InvalidXMLException, IOException {
		String text = "Je suis né en 1995 et ma soeur le en 2012";
		ResultTest result = new ResultTest();
		result.addAnnot("1995");
		result.addAnnot("2012");
		assertTrue(result.equals(getTimex(text)));
	}

	@Test
	/*
	 * TODO: >>En<< 2004 ne prend en compte que en 2004
	 * TODO: >>Dès<< 2004
	 * TODO: le 30/5
	 * TODO: add date_r7a-relative (mois)
	 * TODO: add (date_r5c-relative (jours)
	 */
	public void testmonthYear()
			throws AnalysisEngineProcessException
			, ResourceInitializationException
			, InvalidXMLException, IOException {
		SimpleLayout layout = new SimpleLayout();
		String text = String.join("\n"
				, "Je suis né en mai 1985"
				, "et ma soeur en juin 2012"
				, "et mon frere en Juin 86"
				, "et mon grand pere le 12/2010"
				, "et mon voisin le 01/12"
				, "et mon voisin le lundi 13 mars"
				, "ça c'est passé lundi."
				, "ça c'est passé le 28-09-17"
				, "en 2001, Mme Brun"
				, "Un séjour du 23 au 25 janvier 2017."
				, "Le 4 oct 2017 "
				, "en avril"
				, "au 20/04"
		);
		ResultTest result = new ResultTest();
		result.addAnnot("mai 1985");
		result.addAnnot("juin 2012");
		result.addAnnot("Juin 86");
		result.addAnnot("12/2010");
		result.addAnnot("01/12");
		result.addAnnot("lundi 13 mars");
		result.addAnnot("28-09-17");
		result.addAnnot("2001");
		result.addAnnot("23");
		result.addAnnot("25 janvier 2017");
		result.addAnnot("4 oct 2017");
		result.addAnnot("avril");
		result.addAnnot("20/04");

		System.out.println(getTimex(text));
		assertTrue(result.equals(getTimex(text)));
	}
}
